'''
Praktikum Digitalisierung

Kalorimetrie - Küchentischversuch


Template Code:
Use this file to measure the ausarbeitung heat capacity
This template can be modified and then also used for ausarbeitung newton

@author: Tahsin Ahmad

'''



from functions import m_json
from functions import m_pck
import os
import json
import shutil
# using the function get_metadata_from_setup
setup_path = "/home/pi/Downloads/calorimetry_home_main/datasheets/setup_newton.json"# change setup for newton
metadata = m_json.get_metadata_from_setup(setup_path)
print()
print()
# indent can be changed if needed i will use 0
print (json.dumps(metadata,indent=0))
print()
print()
# using the function add_temperature_sensor_serials
datasheets_folder_path = "/home/pi/Downloads/calorimetry_home_main/datasheets"
m_json.add_temperature_sensor_serials(datasheets_folder_path, metadata)
print ('METADATA WITH SERIALS')
print (json.dumps(metadata,indent=0))
#using the function archiv_json 
archiv_path = "/home/pi/Downloads/calorimetry_home_main/data/newton"#change path for newton
m_json.archiv_json(datasheets_folder_path, setup_path, archiv_path)
print ('>>>>>> FILES SUCCESSFULLY ADDED INTO THE DATA FOLDER')
print()
print()


####### now i will start with the m_pck folder
#using the function check_sensors
print('NOW USING m_pck FUNCTIONS')
print()
m_pck.check_sensors ()
print()
print()
#using the function get_meas_data_calorimetry
metadata = {
    "sensor": {
        "values": ["sensor_1", "sensor_2"],
        "serials": ["3ce104571a79", "3ce10457690c"],
        "names": ["Red cup sensor_1", "Red cup sensor_2"]# change names according to newton setup
    }
}
#archive path > data folder
#json folder > datasheets_folder_path
data = m_pck.get_meas_data_calorimetry(metadata)
#now using the function logging_calorimetry
m_pck.logging_calorimetry(data, metadata, archiv_path, datasheets_folder_path)
print('YOUR MEASUREMENTS HAVE BEEN COMPLETED THE DATA CAN BE NOW VIEWED IN THE GIVEN FOLDER')




